# Use the official OpenJDK image with Java 17 as the base image
FROM openjdk:17-jdk-slim

# Set the working directory inside the container
WORKDIR /app

# Copy the source code and Maven Wrapper files into the container
COPY . .

# Build the Spring Boot application using Maven Wrapper
RUN ./mvnw clean package -DskipTests

# Expose the port that the Spring Boot application will listen on
EXPOSE 8080

# Specify the command to run the Spring Boot application when the container starts
CMD ["java", "-jar", "target/sensign-1.0.0.jar"]