package sn.map.sensign.controller;

import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.constraints.NotBlank;
import reactor.core.publisher.Mono;
import sn.map.sensign.config.SwaggerConfig;
import sn.map.sensign.service.SignServerService;

@RestController
@RequestMapping("/api")
@Tag(name = "Sign API", description = "Operations for signing documents")
@Validated
public class SignController {

    private final SignServerService signServerService;

    public SignController(SignServerService signServerService) {
        this.signServerService = signServerService;
    }

    /**
     * Sign a document
     * 
     * @param document   the document to be signed
     * @param workerName the worker to use for signing the document
     * @return a ResponseEntity containing the signed document
     */
    @Operation(summary = "Sign a document", security = {
            @SecurityRequirement(name = SwaggerConfig.BEARER_KEY_SECURITY_SCHEME) })
    @ApiResponse(responseCode = "200", description = "Document signed successfully", content = @Content(mediaType = MediaType.MULTIPART_FORM_DATA_VALUE, schema = @Schema(type = "string", format = "binary", description = "Signed document file")))
    @ApiResponse(responseCode = "400", description = "Invalid request parameters")
    @ApiResponse(responseCode = "500", description = "Failed to sign document")
    @PostMapping(value = "/sign", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public Mono<ResponseEntity<byte[]>> signDocument(
            @RequestPart("file") @Validated MultipartFile document,
            @RequestPart("worker") @NotBlank String workerName) {

        return signServerService.signDocument(document, workerName)
                .map(signedDocument -> ResponseEntity.ok()
                        .header("Content-Disposition", "attachment; filename=signed-document.pdf")
                        .contentType(MediaType.APPLICATION_PDF)
                        .body(signedDocument))
                .onErrorResume(error -> {
                    return Mono.just(ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build());
                })
                .switchIfEmpty(Mono.just(ResponseEntity.badRequest().build()));
    }
    /*
     * @Operation(
     * summary = "Get string from private/secured endpoint",
     * security = {@SecurityRequirement(name =
     * SwaggerConfig.BEARER_KEY_SECURITY_SCHEME)})
     * 
     * @GetMapping("/private")
     * public String getPrivateString(JwtAuthenticationToken token) {
     * return "%s, it is private.".formatted(token.getName());
     * }
     */
}
