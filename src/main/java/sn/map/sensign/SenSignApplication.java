package sn.map.sensign;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
 
@SpringBootApplication
public class SenSignApplication {

    public static void main(String[] args) {
        SpringApplication.run(SenSignApplication.class, args);
    }
}
