package sn.map.sensign.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

import reactor.core.publisher.Mono;
import sn.map.sensign.utils.FormDataBodyBuilder;

@Service
public class SignServerService {

    private final WebClient webClient;

    @Value("${signserver.url}")
    private String signServerUrl;

    @Value("${signserver.process}")
    private String signServerProcess = "/process";

    public SignServerService(WebClient.Builder webClientBuilder) {
        this.webClient = webClientBuilder.build();
    }

    public Mono<byte[]> signDocument(byte[] document, String workerName) {
        MultiValueMap<String, Object> formData = new FormDataBodyBuilder()
                .with("workerName", workerName)
                .with("filereceivefile", document, "document.pdf")
                .build();

        return webClient.post()
                .uri(signServerUrl + signServerProcess)
                .contentType(MediaType.MULTIPART_FORM_DATA)
                .body(BodyInserters.fromMultipartData(formData))
                .retrieve()
                .bodyToMono(byte[].class);
    }

    public Mono<byte[]> signDocument(MultipartFile document, String workerName) {
        MultiValueMap<String, Object> formData = new FormDataBodyBuilder()
                .with("workerName", workerName)
                .with("filereceivefile", document.getResource())
                .build();

        return webClient.post()
                .uri(signServerUrl + signServerProcess)
                .contentType(MediaType.MULTIPART_FORM_DATA)
                .body(BodyInserters.fromMultipartData(formData))
                .retrieve()
                .bodyToMono(byte[].class);
    }
}
