package sn.map.sensign.utils;

import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.util.MultiValueMap;

import java.util.Map;

public class FormDataBodyBuilder {

    private final MultiValueMap<String, Object> formData;

    public FormDataBodyBuilder() {
        this.formData = new org.springframework.util.LinkedMultiValueMap<>();
    }

    public FormDataBodyBuilder with(String key, Object value) {
        formData.add(key, value);
        return this;
    }

    public FormDataBodyBuilder with(String key, byte[] value, String filename) {
        Resource resource = new ByteArrayResource(value) {
            @Override
            public String getFilename() {
                return filename;
            }

            @Override
            public long contentLength() {
                return value.length;
            }
        };
        formData.add(key, resource);
        return this;
    }
        public FormDataBodyBuilder withS(String key, byte[] value, String filename) {
        Resource resource = new ByteArrayResource(value) {
            @Override
            public String getFilename() {
                return filename;
            }

            @Override
            public long contentLength() {
                return value.length;
            }
        };
        formData.add(key, resource);
        return this;
    }

    public FormDataBodyBuilder with(Map<String, ?> values) {
        values.forEach(formData::add);
        return this;
    }

    public MultiValueMap<String, Object> build() {
        return formData;
    }
}


